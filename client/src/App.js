import React, { Component } from 'react';
import Header from './components/Header';
import SearchForm from './components/SearchForm';
import HowItWorks from './components/HowItWorks';
import Featured from './components/Featured';
import Categories from './components/Categories';
import Download from './components/DownloadApp';
import SubscribeToEmail from './components/SubscribeToEmail';
import MapLocation from './components/MapLocation';
import Footer from './components/Footer';
import Venues from './components/Venues';
import AddVenue from './components/AddVenue';
import CreateAccount from './components/CreateAccount';
import VenueList from './components/VenueList';
import VenueModal from './components/VenueModal';
import { Container } from 'reactstrap';

import { Provider } from 'react-redux';
import store from './store';

// import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './Normalize.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Header />
          <SearchForm />
          <HowItWorks />
          <Featured />
          <Categories />
          <Download />
          <SubscribeToEmail />
          <MapLocation />
          <Footer />
          <Venues />
          <AddVenue />
          <CreateAccount />
          <Container>
            <VenueModal />
            <VenueList />
          </Container>
        </div>
      </Provider>
    );
  }
}

export default App;
