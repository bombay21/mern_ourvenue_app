export const GET_VENUES = 'GET_VENUES';
export const ADD_VENUE = 'ADD_VENUE';
export const DELETE_VENUE = 'DELETE_VENUE';
export const VENUES_LOADING = 'VENUES_LOADING';
