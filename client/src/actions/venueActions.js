import axios from 'axios';
import { GET_VENUES, ADD_VENUE, DELETE_VENUE, VENUES_LOADING } from './types';

export const getVenues = () => dispatch => {
  dispatch(setVenuesLoading());
  axios.get('/api/venues').then(res =>
    dispatch({
      type: GET_VENUES,
      payload: res.data
    })
  );
};

export const addVenue = venue => dispatch => {
  axios.post('/api/venues', venue).then(res =>
    dispatch({
      type: ADD_VENUE,
      payload: res.data
    })
  );
};

export const deleteVenue = id => dispatch => {
  axios.delete(`/api/venues/${id}`).then(res =>
    dispatch({
      type: DELETE_VENUE,
      payload: id
    })
  );
};

export const setVenuesLoading = () => {
  return {
    type: VENUES_LOADING
  };
};
