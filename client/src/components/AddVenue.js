import React from 'react';
import AppNavbar from './AppNavbar';
import '../stylesheets/AddVenue.css';
import '../stylesheets/signup.css';

const AddVenue = () => {
    return ( 
        <div className="addvenue__body">
            <AppNavbar />
            <div className="form animated fadeIn">

                <ul className="addvenue__tab-group">
                    <li className="tab active">Create new Venue</li>
                </ul>

                <div className="tab-content">
                    <div id="signup">
                        <form className="form-venue">

                            <div className="field-wrap">
                                <input type="text" name="name" autocomplete="off" placeholder="Enter name of venue" />
                            </div>

                            <div className="field-wrap">
                                <select name="category" id="">
                                    <option value="" selected disabled>Select Category</option>
                                    <option>Conference & Business Centres</option>
                                    <option>Arenas & Stadiums</option>
                                    <option>Camps & Retreat Centres</option>
                                    <option>Relaxation & Groove</option>
                                    <option>Cultural & Historical Centres</option>
                                    <option>Cruise Ships</option>
                                    <option>Others</option>
                                </select>
                            </div>

                            <div className="field-wrap">
                                <input type="text" name="location" autocomplete="off" placeholder="Enter address of venue" />
                            </div>

                            <div className="field-wrap">
                                <select name="state" id="">
                                    <option>Select State</option>
                                    <option>Lagos</option>
                                    <option>Enugu</option>
                                    <option>Abuja</option>
                                    <option>Rivers</option>
                                    <option>Akwa-Ibom</option>
                                    <option>Benue</option>
                                    <option>Sokoto</option>
                                    <option>Niger</option>
                                    <option>Borno</option>
                                    <option>Nasarawa</option>
                                </select>
                            </div>

                            <div className="field-wrap upload-group">
                                <label for="file"><i className="far fa-image"></i></label>
                                <input type="file" name="" id="file-upload" style={{display:"none"}} />
                                <input type="text" name="" id="file" placeholder="Upload Image" onfocus="this.blur()" />
                            </div>

                            <p className="status-label mb-none">Status:</p>
                            <div className="field-wrap addvenue__status">
                                <label for="on">Available</label>
                                <input id="on" value="1" name="status" type="radio" checked />
                                <label for="off">Not Available</label>
                                <input id="off" value="0" name="status" type="radio" />
                            </div>
                            <a id="addVenueBtn" className="button button-block"><span className="add-venue-text">Add Venue</span><i className="fas fa-spinner fa-pulse d-none"></i></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default AddVenue;