import React, { Component } from 'react';
import NavbarToggler from './NavbarToggler';
import logo from "../images/svg/branded.svg";

class AppNavbar extends Component {
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      <nav className="p-non-sm">
          <div className="flexed container">
              <a className="brand" href="index.html">
                <img src={logo} width="150" alt="brand_logo" />
              </a>
              <ul>
                  <li className="active"><a className="menu-elements" href="#">Overview</a></li>
                  <li><a className="menu-elements" href="#featured">Featured</a></li>
                  <li><a className="menu-elements" href="#categories">Categories</a></li>
                  <li><a className="venue" href="venues.html">Venues</a></li>
                  <li><a href="create-account.html" className="post-venue"><span className="fa fa-plus"></span> Create
                          Account</a></li>
              </ul>
              <NavbarToggler onClick={this.toggle} />
          </div>
      </nav>
    );
  }
}

export default AppNavbar;
