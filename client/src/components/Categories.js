import React from 'react';
import conference from '../images/svg/conference.svg';
import arena from '../images/svg/pula-arena.svg';
import tent from '../images/svg/tent.svg';
import guitar from '../images/svg/guitar.svg';
import museum from '../images/svg/museum.svg';
import boat from '../images/svg/boat.svg';
const Categories = () => {
    return ( 
        <section id="categories" className="categories mt-xl align-center">
        <div className="container container-slim">
            <h2>Popular Categories</h2>
            <p className="lead">A list of categories that will help you choose your next venue</p>
            <div className="steps pt-lg">
                <div className="category">
                    <figure>
                        <img src={conference} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Conference & Business Centres</h2>
                        <a className="mt-lg" href="#">550 Available Venues</a>
                    </div>
                </div>
                <div className="category">
                    <figure>
                        <img src={arena} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Arenas & Stadiums</h2>
                        <a className="mt-lg" href="#">33 Available Venues</a>
                    </div>
                </div>
                <div className="category">
                    <figure>
                        <img src={tent} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Camps & Retreat Centres</h2>
                        <a className="mt-lg" href="#">70 Available Venues</a>
                    </div>
                </div>
                <div className="category">
                    <figure>
                        <img src={guitar} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Relaxation & Groove</h2>
                        <a className="mt-lg" href="#">876 Available Venues</a>
                    </div>
                </div>
                <div className="category">
                    <figure>
                        <img src={museum} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Cultural & Historical Centres</h2>
                        <a className="mt-lg" href="#">45 Available Venues</a>
                    </div>
                </div>
                <div className="category">
                    <figure>
                        <img src={boat} alt="" />
                    </figure>
                    <div className="detail">
                        <h2 className="mt-none">Cruise Ships</h2>
                        <a className="mt-lg" href="#">21 Available Venues</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
     );
}
 
export default Categories;