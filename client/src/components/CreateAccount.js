import React from 'react';
import AppNavbar from './AppNavbar';
import '../stylesheets/AddVenue.css';
import '../stylesheets/signup.css';

const CreateAccount = () => {
    return ( 
        <div className="addvenue__body">
            <AppNavbar />
            <div className="form animated fadeIn">

                <ul className="tab-group">
                    <li className="tab active"><a href="#signup">Sign Up</a></li>
                    <li className="tab"><a href="#login">Log In</a></li>
                </ul>

                <div className="signup tab-content">
                    <div id="signup">
                        <h2>Sign Up for Free</h2>

                        <form action="/" method="post">

                            <div className="top-row">
                                <div className="field-wrap">
                                    <label>
                                        First Name<span className="req">*</span>
                                    </label>
                                    <input type="text" autocomplete="off" />
                                </div>

                                <div className="field-wrap">
                                    <label>
                                        Last Name<span className="req">*</span>
                                    </label>
                                    <input type="text" autocomplete="off" />
                                </div>
                            </div>

                            <div className="field-wrap">
                                <label>
                                    Email Address<span className="req">*</span>
                                </label>
                                <input type="email" autocomplete="off" />
                            </div>

                            <div className="field-wrap">
                                <label>
                                    Set A Password<span className="req">*</span>
                                </label>
                                <input type="password" autocomplete="off" />
                            </div>
                        <a href="venues.html" className="button button-block">Get Started</a> 

                        </form>

                    </div>

                    <div id="login">
                        <h1>Welcome Back!</h1>

                        <form action="/" method="post">

                            <div className="field-wrap">
                                <label>
                                    Email Address<span className="req">*</span>
                                </label>
                                <input type="email" autocomplete="off" />
                            </div>

                            <div className="field-wrap">
                                <label>
                                    Password<span className="req">*</span>
                                </label>
                                <input type="password" autocomplete="off" />
                            </div>

                            <p className="forgot"><a href="#">Forgot Password?</a></p>

                            <a href="venues.html" className="button button-block">Get Started</a>  

                        </form>

                    </div>

                </div>

            </div> 
        </div>
     );
}
 
export default CreateAccount;