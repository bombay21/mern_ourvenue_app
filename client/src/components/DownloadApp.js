import React from 'react';
import mock from '../images/mock_1.jpg';
import playstore from '../images/svg/playstore.svg';
import apple from '../images/svg/apple.svg';

const Download = () => {
    return ( 
        <section className="container container-slim">
            <div className="download">
                <div className="mocks">
                    <img src={mock} alt="" />
                </div>
                <div className="instruct">
                    <h2>Download <span>OurVenue</span> App Now</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibh euismod tincidunt ut laoreet </p>
                    <div className="app">
                        <a href="#"><img src={playstore} alt="" />Play Store</a>
                        <a href="#"><img src={apple} alt="" /><span>App Store</span></a>
                    </div>
                </div>
            </div>
        </section>
     );
}
 
export default Download;