import React from 'react';
import groove from '../images/groove-venue.jpg';
import beach from '../images/beach-venue-sm.jpg';
import outdoor from '../images/outdoor-venue.jpg';

const Featured = () => {
    return ( 
        <section id="featured" className="container container-slim pt-xl align-center">
            <h2>Featured Venues</h2>
            <p className="custom-lead">See our top pick of venues used by most clients</p>
            <div className="steps featured pt-lg">
                <div className="feature">
                    <div id="" className="item-option close"><i className="fas fa-edit"></i></div>
                    <div id="" className="item-option close"><i className="fas fa-times"></i></div>
                    <a href="#">
                        <figure>
                            <img src={groove} alt="" />
                        </figure>
                        <div className="detail">
                            <h2 id="venue-name" className="mt-none align-left">Zone Tech Park</h2>
                            <p id="venue-address" className="mt-none align-left">Gbagada, Lagos</p>
                        </div>
                    </a>
                    <p className="custom-badge">NEW</p>
                </div>
                <div className="feature">
                    <div id="" className="item-option close"><i className="fas fa-edit"></i></div>
                    <div id="" className="item-option close"><i className="fas fa-times"></i></div>
                    <a href="#">
                        <figure>
                            <img src={beach} alt="" />
                        </figure>
                        <div className="detail">
                            <h2 className="mt-none align-left">Eagle Square</h2>
                            <p className="mt-none align-left">Central Area, Abuja</p>
                        </div>
                    </a>
                </div>
                <div className="feature">
                    <div id="" className="item-option close"><i className="fas fa-edit"></i></div>
                    <div id="" className="item-option close"><i className="fas fa-times"></i></div>
                    <a href="#">
                        <figure>
                            <img src={outdoor} alt="" />
                        </figure>
                        <div className="detail">
                            <h2 className="mt-none align-left">Shell Hall</h2>
                            <p className="mt-none align-left">Port-Harcourt, Rivers</p>
                        </div>
                    </a>
                    <p className="custom-badge">NEW</p>
                </div>
            </div>
        </section>
     );
}
 
export default Featured;