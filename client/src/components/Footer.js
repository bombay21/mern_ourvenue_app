import React from 'react';

const Footer = () => {
    return ( 
        <footer className="footer-venue" style={{ marginTop:-6+'px' }}>
            <div className="container">
                <div className="footer-cols">
                    <ul>
                        <li>About OurVenue</li>
                        <li>Lorem ipsum dolor sit amet,
                            consectetuer adipiscing elit, sed diam
                            nonummy nibh euismod tincidunt ut laoreet
                        </li>
                        <li>
                            <div className="address">
                                <span className="fas fa-map-marker-alt"></span>
                                <address>South Ipsum, Dolor 12A Amet, Consectetuer Adipiscing </address>
                            </div>
                        </li>
                        <li>
                            <div className="address">
                                <span className="fa fa-envelope"></span>
                                <address>kenechukwu.john@gmail.com </address>
                            </div>
                        </li>
                        <li>
                            <div className="address">
                                <span className="fa fa-phone"></span>
                                <address>+234 703 453 8485 </address>
                            </div>
                        </li>
                        <li>
                            <div className="social-icons">
                                <a href="#">
                                    <div className="social-icon-group"><i className="fab fa-facebook-f"></i></div>
                                </a>
                                <a href="#">
                                    <div className="social-icon-group"><span className="fab fa-twitter"></span></div>
                                </a>
                                <a href="#">
                                    <div className="social-icon-group"><span className="fab fa-instagram"></span></div>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li>Navigation</li>
                        <li><a href="#">Browse Venue</a></li>
                        <li><a href="#">Browse Categories</a></li>
                        <li><a href="#">Bookmarks</a></li>
                        <li><a href="#">Venue Request</a></li>
                        <li><a href="#">Contact Agent</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                    <ul>
                        <li>Venues</li>
                        <li><a href="#">Post new Venue</a></li>
                        <li><a href="#">Recent Venues</a></li>
                        <li><a href="#">Venues Listing</a></li>
                        <li><a href="#">Book a Venue</a></li>
                        <li><a href="#">All Categories</a></li>
                    </ul>
                </div>
            </div>
            <div className="footer-base">
                &copy; 2018 OurVenue. Handcrafted by Bombay
            </div>
        </footer>
     );
}
 
export default Footer;