import React, { Component } from 'react';
import AppNavbar from './AppNavbar';

class AppHeader extends Component {
  render() {
    return (
        <header>
            <AppNavbar />
            <div id="showcase" className="m-none p-none">
                <div className="section-main align-center">
                    <h1 className="m-none">Find your next venue</h1>
                    <p className="mt-none">Search through a variety of venues across Nigeria</p>
                </div>
            </div>
        </header>
    );
  }
}

export default AppHeader;
