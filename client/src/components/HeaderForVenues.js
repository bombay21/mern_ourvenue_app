import React, { Component } from 'react';
import AppNavbar from './AppNavbar';

class HeaderForVenues extends Component {
    render() {
        return (
            <header>
                <AppNavbar />
                <div id="showcaseVenue" className="m-none p-none">
                    <div className="section-main align-center">
                        <h1 className="m-none">Your next venue could be here</h1>
                    </div>
                </div>
            </header>
        );
    }
}

export default HeaderForVenues;
