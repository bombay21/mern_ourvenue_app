import React from 'react';
import user from '../images/svg/user.svg';
import seo from '../images/svg/seo.svg';
import booking from '../images/svg/online-booking.svg';

const HowItWorks = () => {
    return ( 
        <section className="container container-slim pt-lg align-center">
            <h2>How it Works</h2>
            <p className="lead">Secure your next venue in three easy steps</p>
            <div className="steps account pt-lg">
                <div className="step create">
                    <figure>
                        <img src={user} alt="" />
                    </figure>
                    <figcaption>
                        <h4>Create an Account</h4>
                    </figcaption>
                    <p>Consectetuer adipi Lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, consectetuer adipi</p>
                </div>
                <div className="step search">
                    <figure>
                        <img src={seo} alt="" />
                    </figure>
                    <figcaption>
                        <h4>Search for a Venue</h4>
                    </figcaption>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipi consectetuer adipi Lorem ipsum dolor sit amet</p>
                </div>
                <div className="step book">
                    <figure>
                        <img src={booking} alt="" />
                    </figure>
                    <figcaption>
                        <h4>Book Venue</h4>
                    </figcaption>
                    <p>Adipi Lorem ipsum dolor sit amet, consectetuer lorem ipsum dolor sit amet, consectetuer adipi</p>
                </div>
            </div>
        </section>
     );
}
 
export default HowItWorks;