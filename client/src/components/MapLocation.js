import React from 'react';

const url = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.4829524009892!2d3.298158114295396!3d6.586731195236481!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b91ca93003309%3A0xa50fda5a6eaf21ee!2sPacific+Comprehensive+College!5e0!3m2!1sen!2sng!4v1538388065576";
const mapDimension = {
    width: '100%',
    height: '450px',
    border: 0
}

const MapLocation = () => {
    return ( 
        <section id="map">
            <iframe src={url} 
            style={mapDimension} 
            frameborder="0"
            allowfullscreen>
            </iframe>
        </section>
     );
}
 
export default MapLocation;