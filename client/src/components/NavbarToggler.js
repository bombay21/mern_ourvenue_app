import React, { Component } from 'react';

class NavbarToggler extends Component {
  render() {
    return (
        <a id="drawer">
            <span className="fa fa-bars"></span>
        </a>
    );
  }
}

export default NavbarToggler;
