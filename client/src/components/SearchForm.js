import React, { Component } from 'react';

class SearchForm extends Component {
    state = {  }
    render() { 
        return ( 
            <section id="search-form" className="form-container">
                <form action="" className="p-sm">
                    <div className="input-control bordered-sm">
                        <i className="fas fa-pencil-alt"></i>
                        <input id="search-field" type="text" placeholder="Enter name of venue" />
                    </div>
                    <div className="input-control bordered bordered-sm">
                        <i className="fas fa-map-marker-alt"></i>
                        <select name="" id="">
                            <option>Select State</option>
                            <option value="1">Lagos</option>
                            <option value="2">Enugu</option>
                            <option value="3">Abuja</option>
                            <option value="4">Rivers</option>
                            <option value="5">Akwa-Ibom</option>
                            <option value="6">Benue</option>
                            <option value="7">Sokoto</option>
                            <option value="8">Niger</option>
                            <option value="9">Borno</option>
                            <option value="10">Nasarawa</option>
                        </select>
                    </div>
                    <div className="input-control bordered-sm">
                        <i className="fas fa-list-alt"></i>
                        <select name="category" id="">
                            <option value="">All Category</option>
                            <option value="">Conference & Business Centres</option>
                            <option value="">Arenas & Stadiums</option>
                            <option value="">Camps & Retreat Centres</option>
                            <option value="">Relaxation & Groove</option>
                            <option value="">Cultural & Historical Centres</option>
                            <option value="">Cruise Ships</option>
                        </select>
                    </div>
                    <div className="input-button">
                        <button><span className="text">Find Venue</span> <span className="fas fa-search"></span> </button>
                    </div>
                </form>
            </section>
         );
    }
}
 
export default SearchForm;