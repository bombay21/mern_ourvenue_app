import React from 'react';

const SubscribeToEmail = () => {
    return ( 
        <section className="subscribe">
            <div className="sub-form container container-slim">
                <form action="">
                    <div className="form-email">
                        <span className="fas fa-envelope"></span>
                        <input type="text" placeholder="Enter your email address" />
                    </div>
                    <div className="form-submit">
                        <button type="button">Subscribe</button>
                    </div>
                </form>
            </div>
        </section>
     );
}
 
export default SubscribeToEmail;