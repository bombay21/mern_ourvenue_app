import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getVenues, deleteVenue } from '../actions/venueActions';
import PropTypes from 'prop-types';

class VenueList extends Component {
  componentDidMount() {
    this.props.getVenues();
  }

  onDeleteClick = id => {
    this.props.deleteVenue(id);
  };

  render() {
    const { venues } = this.props.venue;
    return (
      <Container>
        <ListGroup>
          <TransitionGroup className="shopping-list">
            {venues.map(({ _id, name }) => (
              <CSSTransition key={_id} timeout={500} classNames="fade">
                <ListGroupItem>
                  <Button
                    className="remove-btn"
                    color="danger"
                    size="sm"
                    onClick={this.onDeleteClick.bind(this, _id)}
                  >
                    &times;
                  </Button>
                  {name}
                </ListGroupItem>
              </CSSTransition>
            ))}
          </TransitionGroup>
        </ListGroup>
      </Container>
    );
  }
}

VenueList.propTypes = {
  getVenues: PropTypes.func.isRequired,
  venue: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  venue: state.venue
});

export default connect(
  mapStateToProps,
  { getVenues, deleteVenue }
)(VenueList);
