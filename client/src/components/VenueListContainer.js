import React from 'react';

const VenueListContainer = () => {
    return ( 
        <section id="featured" className="container container-slim pt-xl align-center">
            <h2>Venues</h2>
            <p className="lead">List of all venues</p>
            <div id="ourvenues" className="steps featured pt-lg"></div>
            <div className="pagination-v pt-lg">
                <div>
                    <button id="prev"><i className="fas fa-chevron-left"></i></button>
                    <span id="page-number">Page 1</span>
                    <button id="next"><i className="fas fa-chevron-right"></i></button>
                </div>
                <hr />
                <p id="pages">Showing 1 to 30 of 60000 venues</p>
            </div>
        </section>
    );
}
 
export default VenueListContainer;