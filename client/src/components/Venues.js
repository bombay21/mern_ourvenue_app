import React, { Component } from 'react';
import HeaderForVenues from './HeaderForVenues';
import SearchForm from './SearchForm';
import VenueListContainer from './VenueListContainer';
import Intercom from './Intercom';
import Footer from './Footer';

class Venues extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                <HeaderForVenues />
                <SearchForm />
                <VenueListContainer />
                <Intercom />
                <Footer />
            </div>
        );
    }
}
 
export default Venues;