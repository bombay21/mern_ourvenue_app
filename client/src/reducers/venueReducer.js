import {
  GET_VENUES,
  ADD_VENUE,
  DELETE_VENUE,
  VENUES_LOADING
} from '../actions/types';

const initialState = {
  venues: [],
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_VENUES:
      return {
        ...state,
        venues: action.payload,
        loading: false
      };
    case DELETE_VENUE:
      return {
        ...state,
        venues: state.venues.filter(venue => venue._id !== action.payload)
      };
    case ADD_VENUE:
      return {
        ...state,
        venues: [action.payload, ...state.venues]
      };
    case VENUES_LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
}
