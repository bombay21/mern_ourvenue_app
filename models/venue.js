const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const VenueSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  // category: {
  //   type: String,
  //   required: true
  // },
  // address: {
  //   type: String,
  //   required: true
  // },
  // state: {
  //   type: String,
  //   required: true
  // },
  // status: {
  //   type: 'Number',
  //   required: true
  // },
  // image: {
  //   type: String,
  //   data: Buffer
  // }
});

module.exports = Venue = mongoose.model('venue', VenueSchema);
