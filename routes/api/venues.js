const express = require('express');
const router = express.Router();

// Venue Model
const Venue = require('../../models/Venue');

// @route   GET api/venues
// @desc    Get All Venue
// @access  Public
router.get('/',(req, res) => {
  Venue.find()
    .sort({ date: -1 })
    .then(venues => res.json(venues));
});

// @route   POST api/venues
// @desc    Create An Venue
// @access  Public
router.post('/', (req, res) => {
  const newVenue = new Venue({
    name: req.body.name,
    // category: req.body.category,
    // address: req.body.address,
    // state: req.body.state,
    // status: req.body.status,
    // image: req.body.image
  });

  newVenue.save().then(venue => res.json(venue));
});

// @route   DELETE api/venues/:id
// @desc    Delete An Venue
// @access  Public
router.delete('/:id', (req, res) => {
  Venue.findById(req.params.id)
    .then(venue => venue.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;
